package com.voidream.appname.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

public class LoadingMoreFooter extends LinearLayout {

    private ProgressBar progressCon;
    public final static int STATE_LOADING = 0;
    public final static int STATE_COMPLETE = 1;
    private int h, padding;


	public LoadingMoreFooter(Context context) {
		super(context);
        final float scale = context.getResources().getDisplayMetrics().density;
        h = (int) (32 * scale + 0.5f);
        padding = (int) (16 * scale + 0.5f);

        initView();
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public LoadingMoreFooter(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}
    public void initView(){
        setGravity(Gravity.CENTER);
        setLayoutParams(new RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        progressCon = new ProgressBar(getContext());
        progressCon.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        progressCon.getLayoutParams().height = h;
        progressCon.getLayoutParams().width = h;
        this.setPadding(padding,padding,padding,padding);

        addView(progressCon);
    }

    public void  setState(int state) {
        switch(state) {
            case STATE_LOADING:
                progressCon.setVisibility(View.VISIBLE);
                this.setVisibility(View.VISIBLE);
                    break;
            case STATE_COMPLETE:
                this.setVisibility(View.GONE);
                break;
        }
    }
}
