package com.voidream.appname.data.source.remote

import com.voidream.appname.data.util.helper.ConstantsHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


object RemoteDataSource : com.voidream.appname.data.source.DataSource {

    //Create your api service here
    private val apiService = com.voidream.appname.data.source.remote.ApiService.create(ConstantsHelper.API_URL)

    private val contentType: String = "application/json"


    override fun saveToken(accessToken: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getToken(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveLoginState(email: String, password: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getLoginState(): com.voidream.appname.data.param.LoginParam? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun needReloadData(isNecessary: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isNeedReloadData(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun login(loginParam: com.voidream.appname.data.param.LoginParam, callback: com.voidream.appname.data.source.DataSource.LoginCallback) {
        apiService.login(contentType, loginParam)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : com.voidream.appname.data.source.remote.ApiCallback<com.voidream.appname.base.BaseApiModel<com.voidream.appname.data.model.LoginModel>>() {
                    override fun onSuccess(model: com.voidream.appname.base.BaseApiModel<com.voidream.appname.data.model.LoginModel>) {
                        if (model.code == 200)
                            callback.onSuccess(model.data)
                        else {
                            callback.onError(model.message)
                        }
                    }

                    override fun onFailure(errorMessage: String) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                })
    }

}