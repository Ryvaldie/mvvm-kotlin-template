package id.gits.pwc_digitaloffice.data.model.profile

import com.google.gson.annotations.SerializedName


data class Profile(
        val id: Int,
        val email: String,
        val username: String,
        @SerializedName("name")
        val name: String,
        val nick_name: String,
        val avatar: String,
        val photo: String,
        val contact_person: String,
        @SerializedName("mobile_phone")
        val mobile_phone: String,
        val type: String,
        val status: String,
        val epeople_id: String,
        val is_active: Int,
        @SerializedName("wa_numbers")
        val wa_numbers: String,
        val message: String?,
        val remarks: String,
        val createdAt: String,
        val updatedAt: String
)