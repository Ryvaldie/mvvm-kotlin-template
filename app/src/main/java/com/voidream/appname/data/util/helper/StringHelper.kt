package com.voidream.appname.data.util.helper

import android.telephony.PhoneNumberUtils
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


class StringHelper {


    companion object {
        //TIME & DATE FORMAT
        val DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        val DATE_TIME_FORMAT_NO_TZ = "yyyy-MM-dd HH:mm:ss"
        val DATE_FORMAT_DAY_MONTH_ONLY = "dd MMM"
        val DATE_FORMAT_MONTH_DAY = "MMM dd"
        val DATE_FORMAT_STANDART = "yyyy-MM-dd HH:mm:ss"
        val DATE_FORMAT_DAY_FIRST = "dd-MM-yyyy"
        val DATE_FORMAT_DAY_FIRST_MMM = "dd-MMM-yyyy"
        val DATE_FORMAT_DAY_FIRST_MMM_WITH_TIME = "dd MMM yyyy HH:mm"
        val DATE_FORMAT_YEAR_FIRST = "yyyy-MM-dd"
        val DATE_FORMAT_FULL_DATE = "EEEE, dd MMM yyyy"
        val DATE_FORMAT_FULL_DATE_AND_TIME = "EEEE, dd MMM yyyy HH:MM"

        val TIME_FORMAT_24H = "HH:mm"
        val TIME_FORMAT_AMPM = "hh:mm aa"

        // Image Request Code
        val REQUEST_IMAGE_CAPTURE = 1
        val REQUEST_IMAGE_GALLERY = 2
        val REQUEST_PERMISSION_RESULT = 123

        fun validateEmailPattern(text: String): Boolean {
            val emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
            return emailPattern.matcher(text).matches()
        }

        fun validatePhoneNumberPattern(text: String): Boolean {
            return PhoneNumberUtils.isGlobalPhoneNumber(text)
//            val phoneNumberPattern = Pattern.compile("^\\+[0-9]{10,13}\$")
//            return phoneNumberPattern.matcher(text).matches()
        }

        fun getFirstWord(text: String): String {
            return if (text.indexOf(' ') > -1) { // Check if there is more than one word.
                text.substring(0, text.indexOf(' ')) // Extract first word.
            } else {
                text // Text is the first word itself.
            }
        }

        fun capitalizeFirstCharacter(text: String): String {
            var stringResult = ""
            val tempString = text.split(" ")
            stringResult = tempString.joinToString(" ") {
                it.capitalize()
            }
            return stringResult
        }

        fun uncapitalize(s: String?): String? {
            return if (s != null && s.length > 0) {
                s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase()
            } else
                s
        }

        fun dateFormat(timeMillis: Long, outputFotmat: String, isLocale: Boolean): String {
            val locale: Locale
            val simpleDateFormat: SimpleDateFormat

            if (isLocale) {
                locale = Locale("id", "ID")
                simpleDateFormat = SimpleDateFormat(outputFotmat, locale)
            } else {
                simpleDateFormat = SimpleDateFormat(outputFotmat)
            }

            val calendar = Calendar.getInstance()

            calendar.timeInMillis = timeMillis

            return simpleDateFormat.format(calendar.time)
        }

        fun dateFormat(date: String, inputFormat: String): Long {
            return SimpleDateFormat(inputFormat).parse(date).time
        }


        fun dateFormat(stringDate: String): String {
            if (stringDate.isNullOrEmpty()) {
                return ""
            }

            return if (stringDate.length > 10) {
                stringDate.substring(0, 10)
            } else {
                stringDate
            }
        }

        fun stringToDate(stringDate: String, formatInput: String): Date {
            return if (stringDate.isNullOrEmpty()) {
                Calendar.getInstance().time
            } else {
                SimpleDateFormat(formatInput, Locale.US).parse(stringDate)
            }
        }

        fun dateToString(date: Date, formatOutput: String): String {
            return SimpleDateFormat(formatOutput, Locale.US).format(date)
        }

        fun reformatStringDate(stringDate: String, formatInput: String, formatOutput: String): String {
            return dateToString(stringToDate(stringDate, formatInput), formatOutput)
        }

        fun stringToTime(stringTime: String, formatInput: String): Long {
            return if (stringTime.isNullOrEmpty()) {
                Calendar.getInstance().timeInMillis
            } else {
                SimpleDateFormat(formatInput, Locale.US).parse(stringTime).time
            }
        }

        fun timeToString(time: Long, formatOutput: String): String {
            return SimpleDateFormat(formatOutput, Locale.US).format(time)
        }

        fun reformatStringTime(stringTime: String, formatInput: String, formatOutput: String): String {
            return timeToString(stringToTime(stringTime, formatInput), formatOutput)
        }

        /**
         *
         * @param inputFormat DateHelper.DATE_INPUT_v1
         * @param outputFormat DateHelper.DATE_FORMAT_FULL_DATE
         * @param inputDate 2007-01-01
         * @return Monday, 01 Jan 2007
         */
        fun formateDateFromstring(inputFormat: String, outputFormat: String, inputDate: String): String {
            var parsed: Date? = null
            var outputDate = ""
            val df_input = SimpleDateFormat(inputFormat, java.util.Locale.getDefault())
            val df_output = SimpleDateFormat(outputFormat, java.util.Locale.getDefault())

            try {
                parsed = df_input.parse(inputDate)
                outputDate = df_output.format(parsed)

            } catch (e: ParseException) {
            }

            return outputDate

        }
    }
}