package com.voidream.appname.data.util.helper

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.ContactsContract
import android.widget.Toast
import java.net.URLEncoder


class IntentHelper {
    companion object {
        fun callPhone(activity: Activity, phoneNumber: String) {
            if (!phoneNumber.isNullOrEmpty()) {
                var toNumber = phoneNumber.replace(" ", "").replace("-", "")


                try {
                    val callIntent = Intent(Intent.ACTION_DIAL)
                    callIntent.data = Uri.parse("tel:" + toNumber)
                    activity.startActivity(callIntent)
                } catch (e: PackageManager.NameNotFoundException) {
                    Toast.makeText(activity.applicationContext, "App is not installed on your phone", Toast.LENGTH_SHORT).show();
                }
            }
        }

        fun sendMessage(activity: Activity, phoneNumber: String) {
            if (!phoneNumber!!.isNullOrEmpty()) {
                var toNumber = phoneNumber.replace(" ", "").replace("-", "")

                try {
                    val smsIntent = Intent(Intent.ACTION_VIEW)
                    smsIntent.data = Uri.parse("sms:")
                    smsIntent.type = "vnd.android-dir/mms-sms"
                    smsIntent.putExtra("address", toNumber)
                    smsIntent.putExtra("sms_body", "")
                    activity.startActivity(smsIntent)
                } catch (e: PackageManager.NameNotFoundException) {
                    Toast.makeText(activity.applicationContext, "App is not installed on your phone", Toast.LENGTH_SHORT).show();
                }
            }
        }

        fun contactWhatsapp(activity: Activity, phoneNumber: String) {
            if (!phoneNumber.isNullOrEmpty()) {
                var toNumber = phoneNumber.replace(" ", "").replace("-", "")
                        .replaceFirst("0", "+62")

                if (isAppInstalled(activity, "com.whatsapp")) {
                    val packageManager = activity.packageManager
                    val waIntent = Intent(Intent.ACTION_VIEW)
                    try {
                        val url = "https://api.whatsapp.com/send?phone=" + toNumber + "&text=" + URLEncoder.encode("", "UTF-8")
                        waIntent.`package` = "com.whatsapp"
                        waIntent.data = Uri.parse(url)
                        if (waIntent.resolveActivity(packageManager) != null) {
                            activity.startActivity(waIntent)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }

        fun addToContact(activity: Activity, name: String, phoneNumber: String) {
            if (!name.isNullOrEmpty() && !phoneNumber.isNullOrEmpty()) {
                var toNumber = phoneNumber.replace(" ", "").replace("-", "")

                try {
                    val addContactIntent = Intent(Intent.ACTION_INSERT)
                    addContactIntent.type = ContactsContract.Contacts.CONTENT_TYPE
                    addContactIntent.putExtra(ContactsContract.Intents.Insert.NAME, name)
                    addContactIntent.putExtra(ContactsContract.Intents.Insert.PHONE, toNumber)
                    activity.startActivity(addContactIntent)
                } catch (e: PackageManager.NameNotFoundException) {
                    Toast.makeText(activity.applicationContext, "App is not installed on your phone", Toast.LENGTH_SHORT).show();
                }
            }
        }

        private fun isAppInstalled(activity: Activity, uri: String): Boolean {
            val pm = activity.packageManager
            return try {
                pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
                true
            } catch (e: PackageManager.NameNotFoundException) {
                Toast.makeText(activity.applicationContext, "App is not installed on your phone", Toast.LENGTH_SHORT).show();
                false
            }
        }
    }
}