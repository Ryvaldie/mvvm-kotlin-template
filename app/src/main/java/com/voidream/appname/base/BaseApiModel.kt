package com.voidream.appname.base


data class BaseApiModel<T>(val code: Int, val message: String, val data: T)
//        var page: Int,
//        var total_results: Int,
//        var total_pages: Int,
//        var results: T? = null
//
//        //Todo code above just for testing. Change it with real base response from API