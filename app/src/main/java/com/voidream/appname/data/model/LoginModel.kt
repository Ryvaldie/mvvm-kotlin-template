package com.voidream.appname.data.model

import com.google.gson.annotations.SerializedName
import id.gits.pwc_digitaloffice.data.model.profile.Profile

data class LoginModel(
        @SerializedName("token")
        val token: String? = null,
        @SerializedName("payload")
        val payload: Profile
)