package com.voidream.appname.mvvm

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.voidream.appname.R
import com.voidream.appname.data.util.extension.obtainPeopleViewModel
import com.voidream.appname.data.util.extension.replaceFragmentInActivity


class MainActivity : com.voidream.appname.base.BaseActivity() {

    private lateinit var mainViewModel: MainViewModel

    private var exit: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        setupToolbar()
//        setupMenuToolbar(R.id.action_home)
        setupFragment()
        setupViewModel()
    }

    override fun onBackPressed() {
        if (exit) {
            finish()
            System.exit(0)
        } else {
            Toast.makeText(this, R.string.back_to_exit, Toast.LENGTH_SHORT).show()
            exit = true
            Handler().postDelayed({ exit = false }, (3 * 1000).toLong())
        }
    }

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or
                    Intent.FLAG_ACTIVITY_NEW_TASK or
                    Intent.FLAG_ACTIVITY_CLEAR_TASK))
        }

    }

    /*private fun setupToolbar() {
        setSupportActionBar(activity_main_toolbar)
    }*/

    /*private fun setupOnClickMenu() {
        activity_main_toolbar_search.setOnClickListener {
            when {
                bottom_navigation.menu.getItem(1).isChecked -> SearchActivity.startActivity(this, ConstantsHelper.SEARCH_PEOPLE)
                else -> Toast.makeText(applicationContext, "UNDER CONSTRUCTION", Toast.LENGTH_SHORT).show()
            }
        }
    }*/

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.activity_main_frame)
        MainFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.activity_main_frame)
        }
    }

    fun obtainViewModelMain(): MainViewModel = obtainPeopleViewModel(MainViewModel::class.java)

    private fun setupViewModel() {
        mainViewModel = obtainViewModelMain()
    }

}