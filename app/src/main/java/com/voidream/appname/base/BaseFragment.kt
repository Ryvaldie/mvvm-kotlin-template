package com.voidream.appname.base

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.voidream.appname.R


open class BaseFragment : Fragment() {

    private var mDialogProgress: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupProgressDialog()
    }

    /**
     * Do setup progress dialog for init
     */
    private fun setupProgressDialog() {

        mDialogProgress = Dialog(activity)
        mDialogProgress?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialogProgress?.setContentView(R.layout.progress_dialog)
        mDialogProgress?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    /**
     * Show this dialog when the app request to API.
     * Hide it when the opposite
     * @param mMessage
     */
    fun showProgressDialog(isShowDialog: Boolean) {

        if (isShowDialog) {
            mDialogProgress?.show()
        } else {
            mDialogProgress?.dismiss()
        }
    }

    fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun hideSoftKeyboard(activity: Activity) {
        val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        val focusedView = activity.currentFocus
        if (focusedView != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }
}