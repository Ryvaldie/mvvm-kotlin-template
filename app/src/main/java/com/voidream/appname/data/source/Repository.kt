package com.voidream.appname.data.source

/**
 * Created by IDUA on 22/01/2018.
 */

open class Repository(val remoteDataSource: com.voidream.appname.data.source.DataSource, val localDataSource: com.voidream.appname.data.source.DataSource) : com.voidream.appname.data.source.DataSource {


    override fun login(loginParam: com.voidream.appname.data.param.LoginParam, callback: com.voidream.appname.data.source.DataSource.LoginCallback) {
        remoteDataSource.login(loginParam, object : com.voidream.appname.data.source.DataSource.LoginCallback {
            override fun onSuccess(loginCallback: com.voidream.appname.data.model.LoginModel) {
                callback.onSuccess(loginCallback)
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun onError(message: String?) {
                callback.onError(message)
            }
        })
    }

    override fun saveToken(accessToken: String?) {
        localDataSource.saveToken(accessToken)
    }

    override fun getToken(): String {
        return localDataSource.getToken()
    }

    override fun saveLoginState(email: String, password: String) {
        localDataSource.saveLoginState(email, password)
    }

    override fun getLoginState(): com.voidream.appname.data.param.LoginParam? {
        return localDataSource.getLoginState()
    }

    override fun needReloadData(isNecessary: Boolean) {
        localDataSource.needReloadData(isNecessary)
    }

    override fun isNeedReloadData(): Boolean {
        return localDataSource.isNeedReloadData()
    }

    companion object {
        private var INSTANCE: Repository? = null
        /**
         * Returns the single instance of this class, creating it if necessary.

         * @param RemoteDataSource backend data source
         * *
         * @return the [Repository] instance
         */
        @JvmStatic
        fun getInstance(remoteDataSource: com.voidream.appname.data.source.DataSource, localDataSource: com.voidream.appname.data.source.DataSource) =
                INSTANCE ?: synchronized(Repository::class.java) {
                    INSTANCE ?: Repository(remoteDataSource, localDataSource)
                            .also { INSTANCE = it }
                }

        /**
         * Used to force [getInstance] to create a new instance
         * next time it's called.
         */
        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}