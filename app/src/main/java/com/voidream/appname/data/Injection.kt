package com.voidream.appname.data

import android.content.Context
import android.preference.PreferenceManager

/**
 * Created by IDUA on 22/01/2018.
 */

object Injection{
    fun providePriceWaterRepository(context: Context) =
            com.voidream.appname.data.source.Repository.getInstance(com.voidream.appname.data.source.remote.RemoteDataSource,
                    com.voidream.appname.data.source.local.LocalDataSource.getInstance(PreferenceManager.getDefaultSharedPreferences(context)))
}