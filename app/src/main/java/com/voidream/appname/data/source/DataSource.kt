package com.voidream.appname.data.source


interface DataSource {

    /*
    * Create your function and interface here
    * */

    fun login(loginParam: com.voidream.appname.data.param.LoginParam, callback: LoginCallback)
    fun saveToken(accessToken: String?)
    fun getToken(): String
    fun saveLoginState(email: String, password: String)
    fun getLoginState(): com.voidream.appname.data.param.LoginParam?
    fun needReloadData(isNecessary: Boolean)
    fun isNeedReloadData(): Boolean

    interface LoginCallback {
        fun onSuccess(loginCallback: com.voidream.appname.data.model.LoginModel)
        fun onFinish()
        fun onError(message: String?)
    }

}