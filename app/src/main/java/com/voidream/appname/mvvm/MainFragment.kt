package com.voidream.appname.mvvm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.voidream.appname.databinding.FragmentMainBinding


class MainFragment : com.voidream.appname.base.BaseFragment() {
    private lateinit var viewBinding: FragmentMainBinding

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        viewBinding = FragmentMainBinding.inflate(inflater!!, container, false).apply {
            mViewModel = (activity as MainActivity).obtainViewModelMain()
        }

        return viewBinding.root
    }

    override fun onResume() {
        super.onResume()
    }

    companion object {

        fun newInstance() = MainFragment().apply {
        }
    }
}