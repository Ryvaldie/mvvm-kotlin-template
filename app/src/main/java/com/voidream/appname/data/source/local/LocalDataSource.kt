package com.voidream.appname.data.source.local

import android.content.SharedPreferences
import android.support.annotation.VisibleForTesting


class LocalDataSource private constructor(private val pref: SharedPreferences) : com.voidream.appname.data.source.DataSource {

    private val KEY_TOKEN = "TOKEN"
    private val KEY_EMAIL = "EMAIL"
    private val KEY_PASSWORD = "PASSWORD"
    private val KEY_IS_NEED_RELOAD_DATA = "IS_NEED_RELOAD_DATA"
    private val KEY_PROFILE_DATA = "PROFILE_DATA"

    // Example
    /*
    * Login, save/get login state, save/get token, synchronisation state (offline/online mode)
    * */
    override fun login(loginParam: com.voidream.appname.data.param.LoginParam, callback: com.voidream.appname.data.source.DataSource.LoginCallback) {
        /*
        * Get synchronisation
        * do accordingly
        * */
    }

    override fun saveToken(accessToken: String?) {
        pref.edit().putString(KEY_PROFILE_DATA, "").apply()
        if (accessToken.isNullOrEmpty()) {
            // Log Out
            pref.edit().putString(KEY_TOKEN, "").apply()
        } else {
            // Log In
            pref.edit().putString(KEY_TOKEN, accessToken).apply()
        }
    }

    override fun getToken(): String {
        return pref.getString(KEY_TOKEN, "")
    }

    override fun saveLoginState(email: String, password: String) {
        pref.edit().putString(KEY_EMAIL, email).apply()
        pref.edit().putString(KEY_PASSWORD, password).apply()
    }

    override fun getLoginState(): com.voidream.appname.data.param.LoginParam? {
        val email = pref.getString(KEY_EMAIL, "")
        val password = pref.getString(KEY_PASSWORD, "")
        return if (email == "" && password == "") null else com.voidream.appname.data.param.LoginParam(email, password)
    }

    override fun needReloadData(isNecessary: Boolean) {
        pref.edit().putBoolean(KEY_IS_NEED_RELOAD_DATA, isNecessary).apply()
    }

    override fun isNeedReloadData(): Boolean {
        return pref.getBoolean(KEY_IS_NEED_RELOAD_DATA, false)
    }

    companion object {
        //Declaration
        private var INSTANCE: LocalDataSource? = null

        @JvmStatic
        fun getInstance(pref: SharedPreferences): LocalDataSource {
            if (INSTANCE == null) {
                synchronized(LocalDataSource::javaClass) {
                    INSTANCE = LocalDataSource(pref)
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }
}
