package com.voidream.appname.data.source.remote

import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

/**
 * Created by IDUA on 22/01/2018.
 */

interface ApiService {

    companion object Factory {
        fun create(baseUrl: String): ApiService {
            val mLoggingInterceptor = HttpLoggingInterceptor()
            mLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val mClient = OkHttpClient.Builder()
                    .addInterceptor(mLoggingInterceptor)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build()

            val mRetrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(mClient) //Todo comment if app release
                    .build()

            return mRetrofit.create(ApiService::class.java)
        }
    }

    /*
    * Create your endpoint here
    * */


    @POST("/v1/auth/login")
    fun login(
            @Header("Content-Type") contentType: String,
            @Body loginParam: com.voidream.appname.data.param.LoginParam): Observable<com.voidream.appname.base.BaseApiModel<com.voidream.appname.data.model.LoginModel>>

    //example
    /*@GET("/v1/users/me")
    fun myProfile(
            @Header("accessToken") accesToken: String): Observable<BaseApiModel<MyProfileModel>>*/

}