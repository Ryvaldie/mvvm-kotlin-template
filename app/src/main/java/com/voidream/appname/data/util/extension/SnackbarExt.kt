package com.voidream.appname.data.util.extension

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.Toast
import id.gits.pwc_digitaloffice.SingleLiveEvent

/**
 * Created by IDUA on 23/01/2018.
 */
//VIEW EKSTENSION
/**
 * Transforms static java function Snackbar.make() to an extensionNumber function on View.
 */
fun View.showSnackbar(snackbarText: String, timeLength: Int) {
    try {
        Snackbar.make(this, snackbarText, timeLength).show()
    } catch (e: Exception) {
        Toast.makeText(context, snackbarText, Toast.LENGTH_SHORT).show()
    }
}

/**
 * Triggers a snackbar message when the value contained by snackbarTaskMessageLiveEvent is modified.
 */
fun View.setupSnackbar(lifecycleOwner: LifecycleOwner,
                       snackbarMessageLiveEvent: SingleLiveEvent<Int>, timeLength: Int) {
    snackbarMessageLiveEvent.observe(lifecycleOwner, Observer {
        it?.let { showSnackbar(context.getString(it), timeLength) }
    })
}

/**
 * Triggers a snackbar message when the value contained by snackbarTaskMessageLiveEvent is modified.
 */
fun View.setupSnackbarRemote(lifecycleOwner: LifecycleOwner,
                             snackbarMessageLiveEvent: SingleLiveEvent<String>, timeLength: Int) {
    snackbarMessageLiveEvent.observe(lifecycleOwner, Observer {
        it?.let { showSnackbar(it, timeLength) }
    })
}