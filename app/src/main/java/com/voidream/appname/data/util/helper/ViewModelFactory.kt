package com.voidream.appname.data.util.helper

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.annotation.VisibleForTesting
import com.voidream.appname.mvvm.MainViewModel

/**
 * Created by IDUA on 22/01/2018.
 */
class ViewModelFactory private constructor(
        private val application: Application,
        private val gitsRepository: com.voidream.appname.data.source.Repository
) : ViewModelProvider.NewInstanceFactory() {


    //Add your viewmodel here
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                   isAssignableFrom(MainViewModel::class.java) ->
                        MainViewModel(application, gitsRepository)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var INSTANCE: ViewModelFactory? = null

        fun getInstance(application: Application) =
                INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                    INSTANCE ?: ViewModelFactory(application,
                            com.voidream.appname.data.Injection.providePriceWaterRepository(application.applicationContext))
                            .also { INSTANCE = it }
                }


        @VisibleForTesting
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}