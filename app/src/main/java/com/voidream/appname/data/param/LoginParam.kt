package com.voidream.appname.data.param

import com.google.gson.annotations.SerializedName

data class LoginParam(
        @SerializedName("email")
        val email: String? = "-",
        @SerializedName("password")
        val password: String? = null
)